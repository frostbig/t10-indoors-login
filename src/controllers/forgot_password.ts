import mongoose from 'mongoose';
import UserSchema from '../mongo/schemas/user';
import { encrypt, randomHex } from '../utils/crypto';
import { USER_NOT_FOUND, SERVER_ERR, EMAIL_SENT } from '../constants/server_responses';
import transporter from '../utils/sendEmail';
import { EMAIL } from '../constants/email';

const data_base = mongoose.model('User', UserSchema.schemas);

const forgotPass = async (user_email: string) => {
    const email = user_email.toLowerCase()
    const data: any = await data_base.findOne({ email: email })

    if (data !== null) {
        const token = randomHex(5);

        await data_base.findOneAndUpdate({ email: email }, { tokenReset: encrypt(token) })
        const hashToken = encrypt(JSON.stringify(data._id))

        const mailOptions = {
            from: EMAIL,
            to: 'frostbigbig@hotmail.com',  //CHANGE EMAIL
            subject: 'Reset password',
            text: `siga o link: http://localhost:3001/reset-password  seu token para a troca de senha é :${token}`
        };

        transporter.sendMail(mailOptions, (err, info) => {
            if (err) {
                return { status: 400, response: { msg: SERVER_ERR.pt_br } }
            }
        });

        return { status: 200, response: { msg: EMAIL_SENT.pt_br, tokenReset: hashToken } }
    }
    return { status: 400, response: { msg: USER_NOT_FOUND.pt_br } }
}

export default forgotPass;