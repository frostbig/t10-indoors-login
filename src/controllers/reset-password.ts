import mongoose from 'mongoose';
import { encrypt, decrypt } from '../utils/crypto';
import UserSchema from '../mongo/schemas/user';
import { INVALID_TOKEN, NEW_PASSWORD_SAVED, WITHOUT_AUTHORIZATION } from '../constants/server_responses'

const data_base = mongoose.model('User', UserSchema.user)

const resetPassword = async (token: string, tokenReset: string, newPassword: string) => {

  if (tokenReset === '') { return { status: 400, response: { msg: WITHOUT_AUTHORIZATION.pt_br } } }

  const idUser = decrypt(JSON.parse(tokenReset))
  const find = await data_base.findById(JSON.parse(idUser))
  const tokenValidate = decrypt(find.tokenReset)

  if (tokenValidate === token) {

    await data_base.findOneAndUpdate({ _id: JSON.parse(idUser) }, { password: encrypt(newPassword) })
    //await data_base.findOneAndUpdate({_id: JSON.parse(idUser)}, {tokenReset: ''})
    return { status: 200, response: { msg: NEW_PASSWORD_SAVED.pt_br } }
  }

  return { status: 400, response: { msg: INVALID_TOKEN.pt_br } }
}

export default resetPassword;