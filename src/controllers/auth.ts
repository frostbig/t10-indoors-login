import mongoose from 'mongoose';
import UserSchema from '../mongo/schemas/user';
import { encrypt, decrypt } from '../utils/crypto';
import { Request } from 'express';
import UserData from '../interfaces/user_data';
import {WRONG_EMAIL, WRONG_PASSWORD, VALID_INFORMATION} from '../constants/server_responses'

const data_base = mongoose.model('User', UserSchema.schema)

const login_validation = async (user_data: UserData, req: Request) => {
   const data: any = await data_base.findOne({ email: user_data.email.toLowerCase() })
   let correctPassword: boolean;
   if (data == null) {
      return {status: 401, response: {msg: WRONG_EMAIL.pt_br}}      
   }

   if (data !== null) {
      const password = user_data.password
      const verifyPassword = decrypt(data.password)
      correctPassword = password == verifyPassword ? true : false
   }

   if (correctPassword) {
      const token = encrypt(data.email)
      req.session.token = token
      return {status : 200, response: {token: token, msg: VALID_INFORMATION.pt_br}}
   }
   return {status:403 ,response: {msg: WRONG_PASSWORD.pt_br}}
}

export default login_validation