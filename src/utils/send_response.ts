import { Response } from "express";

const send_response = (status: number, response: object, res: Response) => {
    res.status(status)
    res.json({
        status,
        response
    });
};
export default send_response;