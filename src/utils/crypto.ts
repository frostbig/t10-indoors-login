import {HASH} from '../constants/hash';
import crypto from 'crypto-js';
import random from 'crypto'

export const encrypt = (msg : string): string=>{
    const result = crypto.AES.encrypt(msg, HASH);
    return result.toString()
}
export const decrypt =(msg : string ): string=>{
    const result = crypto.AES.decrypt(msg.toString(), HASH)
    const decrypted = result.toString(crypto.enc.Utf8)
    return decrypted 
}

export const randomHex = (length: number):any => {
    return random
    .randomBytes(Math.ceil(length / 2))
    .toString('hex')
    .slice(0, length)
}

