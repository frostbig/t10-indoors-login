import mongoose from 'mongoose';
import {URL} from '../constants/mongo'

const connect = async () => {
    await mongoose.connect(URL, {
        dbName: 'Login', 
        useNewUrlParser: true, 
        useUnifiedTopology: true,
        useFindAndModify: false
    });
    console.log("|  Connected to db                    |")
}
export default connect;