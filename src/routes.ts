import { Router } from 'express';
import auth from './routes/auth';
import forgotPassword from './routes/forgotPassword';
import resetPassword from './routes/resetPassword';

const router: Router = Router();

router.use('/auth', auth)
router.use('/forgot-password', forgotPassword)
router.use('/reset-password', resetPassword)
export default router;