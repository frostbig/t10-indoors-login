import app from './server';

const PORT: string = process.env.PORT || '3000'

app.listen(PORT, (): void => {
    console.log(`|  Running on http://localhost:${PORT}   |`)
});
