import { Router, Request, Response } from 'express';
import resetPassword from '../controllers/reset-password';
import send_response from '../utils/send_response';
const routes: Router = Router();

routes.post('/', async(req: Request, res: Response) => {
    const data = req.body
    const info = await resetPassword(data.token, data.tokenReset, data.password)
    console.log(info)
    send_response(info.status, info.response, res)
})


export default routes;