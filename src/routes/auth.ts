import { Router, Request, Response } from 'express';
import login_validation from '../controllers/auth';
import send_response from '../utils/send_response';
const routes: Router = Router();

routes.post('/', async (req: Request, res: Response) => {
    const user_data = req.body
    const info = await login_validation(user_data, req)
    send_response(info.status, info.response, res)
});

export default routes