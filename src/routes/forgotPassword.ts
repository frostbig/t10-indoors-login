import { Router, Request, Response } from 'express';
import forgotPass from '../controllers/forgot_password';
import send_response from '../utils/send_response';
const routes: Router = Router();

routes.post('/', async (req: Request, res: Response) => {
    const {email} = req.body;
    const data = await forgotPass(email)
    send_response(data.status, data.response, res)
})

export default routes;