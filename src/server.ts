import express from 'express';
import routes from './routes';
import bodyParser from 'body-parser';
import session from 'express-session';
import connect from './mongo/connection';
import cors from 'cors';

class App {
    public express: any;

    constructor() {
        this.express = express();
        this.middlewares();
        this.routes();
    }

    middlewares() {
        this.express.use(cors())
        this.express.use(express.urlencoded({ extended: false }));
        this.express.use(bodyParser.json());
        this.express.use(session({
            secret: 'keybord cat',
            resave: false,
            saveUninitialized: true
        }))
        connect();
    }

    routes() {
        this.express.use(routes)
    }
}
export default new App().express;