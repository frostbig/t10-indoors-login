export const WRONG_EMAIL = {
    pt_br: 'O email informado nao está cadastrado',
    english: 'The email you entered is not registered'
}

export const WRONG_PASSWORD = {
    pt_br: 'A senha esta incorreta, por favor verifique e tente novamente',
    english: 'The password is incorrect please check and try again'
}

export const VALID_INFORMATION = {
    pt_br: 'Acesso concedido',
    english: 'Access granted'
}

export const USER_NOT_FOUND = {
    pt_br: 'O usuário não existe no banco de dados',
    english: 'The user does not exist in the database'
}

export const SERVER_ERR = {
    pt_br: 'Houve um erro no server, por favor tente mais tarde',
    english: 'There was an error on the server, please try later'
}

export const EMAIL_SENT = {
    pt_br: 'O email foi enviado. Por favor verifique sua caixa de entrada para continuar',
    english: 'The email has been sent. Please check your inbox to continue'
}
export const WITHOUT_AUTHORIZATION = {
    pt_br: 'Você não possui autorição para completar essa ação',
    english: 'You do not have the authorization to complete this action'
}

export const INVALID_TOKEN = {
    pt_br: 'O token usado não é valido',
    english: 'The used token is not valid'
}
export const NEW_PASSWORD_SAVED = {
    pt_br: 'A nova senha foi salva com sucesso',
    english: 'The new password was saved successfully'
}